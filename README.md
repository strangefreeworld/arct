# arct

Python command line archiving tool

This is a simple command line tool I have made to make it easier to extract archive formats from the command line. You can list, extract and compress tarballs, Zip files, and 7-zip files.

## Dependencies

The following Python packages need to be installed:
 * click
 * py7zr