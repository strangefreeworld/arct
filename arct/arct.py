# Info links:
#    https://thispointer.com/python-how-to-get-the-list-of-all-files-in-a-zip-archive/
#    https://www.askpython.com/python-modules/tarfile-module
#    https://stackhowto.com/how-to-extract-a-zip-file-in-python/
#    https://stackoverflow.com/questions/25960580/fastest-way-to-extract-tar-files-using-python

import tarfile
import zipfile
from pathlib import Path
import shutil
import click
import os
from os import PathLike
from typing import Union
from enum import Enum
import contextlib
import py7zr


class ArchiveType(Enum):
    SEVENZIP_FILE = "7z"
    ZIP_FILE = "zip"
    TARBALL = "tar"
    NONE = "None"


def archive_type(filename):
    if zipfile.is_zipfile(filename):
        return ArchiveType.ZIP_FILE
    if tarfile.is_tarfile(filename):
        return ArchiveType.TARBALL
    try:
        testfile = py7zr.SevenZipFile(filename)
        return ArchiveType.SEVENZIP_FILE
    except py7zr.exceptions.Bad7zFile:
        pass
    return ArchiveType.NONE


def get_parent(file_path):
    return Path(file_path).absolute().parents[0]


def sevenzip_dir(filename, source_dir):
    with py7zr.SevenZipFile(filename, "w") as archive:
        archive.writeall(f"{source_dir}/", arcname=os.path.split(source_dir)[1])


def sevenzip_extract(filename, outputdir=None):
    with py7zr.SevenZipFile(filename, 'r') as archive:
        if outputdir == None:
            archive.extractall()
        else:
            archive.extractall(outputdir)
        

# from https://stackoverflow.com/questions/1855095/how-to-create-a-zip-archive-of-a-directory
def zip_dir(zip_name: str, source_dir: Union[str, PathLike]):
    src_path = Path(source_dir).expanduser().resolve(strict=True)
    with zipfile.ZipFile(zip_name, 'w', zipfile.ZIP_DEFLATED) as zf:
        for file in src_path.rglob('*'):
            zf.write(file, file.relative_to(src_path.parent))


def make_tarfile(output_filename, source_dir, type):
    with tarfile.open(output_filename, type) as tar:
        tar.add(source_dir, arcname=os.path.basename(source_dir))            


def valid_archive(file):
    if zipfile.is_zipfile(file):
        return True
    if tarfile.is_tarfile(file):
        return True
    with contextlib.suppress(py7zr.exceptions.Bad7zFile):
        testfile = py7zr.SevenZipFile(file)
        return True
    return False


def sevenzip_default_list(item):
    click.echo(item.filename)


def sevenzip_verbose_list(item):
    click.echo(f"{item.filename}\t {item.compressed:,}\t {item.uncompressed:,}")


def sevenzip_list(file, verbose):
    list_function = sevenzip_verbose_list if verbose else sevenzip_default_list
    with py7zr.SevenZipFile(file) as archive:
        for item in archive.list():
            list_function(item)


def zipfile_list(file, verbose):
    with zipfile.ZipFile(file, 'r') as zip_file:
        if verbose:
            for item in zip_file.infolist():
                click.echo(f"{item.filename}\t {item.file_size:,}\t {item.compress_size:,}")            
        else:
            for item in zip_file.namelist():
                click.echo(item)


def tarfile_list(file):
    with tarfile.open(file, 'r') as tar_obj:
        for item in tar_obj.getnames():
            click.echo(item)


def list_archive(file, verbose):
    if archive_type(file) is ArchiveType.NONE:
        click.echo(f"File {file} is not a valid achive")
        return
    extension = Path(file).suffix
    if extension in ['.zip', '.whl', '.docx', '.xlsx']:
        zipfile_list(file, verbose)
    elif extension == ".7z":
        sevenzip_list(file, verbose)
    else:
        tarfile_list(file)


def expand_archive(file, output_dir):
    click.echo(f"Extracting {file} to directory {output_dir if output_dir else str(Path.cwd())}")
    arctype = archive_type(file)
    if arctype is ArchiveType.NONE:
        click.echo(f"File {file} is not a valid achive")
        return
    if output_dir:
        Path(output_dir).mkdir(parents=True, exist_ok=True)
        if arctype == ArchiveType.SEVENZIP_FILE:
            sevenzip_extract(file, output_dir)
        else:
            shutil.unpack_archive(file, output_dir)
    else:
        if arctype == ArchiveType.SEVENZIP_FILE:
            sevenzip_extract(file)
        else:
            shutil.unpack_archive(file)


@click.group()
def arct():
    pass


@arct.command()
@click.argument("file", type=click.Path(exists=True))
@click.option('-v', '--verbose', is_flag=True, default=False, show_default=True)
def list(file, verbose):
    list_archive(file, verbose)


@arct.command()
@click.argument("file", type=click.Path(exists=True))
@click.option("-o", "--output-dir", type=str)
def extract(file, output_dir):
    expand_archive(file, output_dir)


@arct.command()
@click.argument("file", type=click.Path(exists=False))
@click.argument("format", type=click.Choice(['7z', 'zip', 'targz', 'tarbz2'], case_sensitive=False))
@click.argument("folder", type=click.Path(exists=True))
def compress(file, format, folder):
    click.echo(f"Create file {file} in format{format} from folder {folder}")
    if format.lower() == "7z":
        click.echo("Making 7z")
        sevenzip_dir(file, folder)
    elif format.lower() == "zip":
        zip_dir(file, folder)
    else:
        tar_format = "w.gz" if format == "targz" else "w:bz2"
        make_tarfile(file, folder, tar_format)
